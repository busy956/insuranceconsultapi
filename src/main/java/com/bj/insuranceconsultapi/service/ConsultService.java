package com.bj.insuranceconsultapi.service;

import com.bj.insuranceconsultapi.entity.Consult;
import com.bj.insuranceconsultapi.model.ConsultItem;
import com.bj.insuranceconsultapi.model.ConsultRequest;
import com.bj.insuranceconsultapi.model.ConsultResponse;
import com.bj.insuranceconsultapi.repository.ConsultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultService {
    private final ConsultRepository consultRepository;

    public void setConsult(ConsultRequest request) {
        Consult addData = new Consult();
        addData.setConsultDate(request.getConsultDate());
        addData.setName(request.getName());
        addData.setBirth(request.getBirth());
        addData.setIsMan(request.getIsMan());
        addData.setConTime(request.getConTime());
        addData.setPhoneNumber(request.getPhoneNumber());

        consultRepository.save(addData);
    }

    public List<ConsultItem> getConsults() {
        List<Consult> originList = consultRepository.findAll();

        List<ConsultItem> result = new LinkedList<>();

        for (Consult consult : originList) {
            ConsultItem addItem = new ConsultItem();
            addItem.setId(consult.getId());
            addItem.setConsultDate(consult.getConsultDate());
            addItem.setName(consult.getName());
            addItem.setBirth(consult.getBirth());
            addItem.setIsMan(consult.getIsMan());

            result.add(addItem);
        }

        return result;
    }

    public ConsultResponse getconsult(long id) {
        Consult originData = consultRepository.findById(id).orElseThrow();

        ConsultResponse response = new ConsultResponse();
        response.setId(originData.getId());
        response.setConsultDate(originData.getConsultDate());
        response.setName(originData.getName());
        response.setBirth(originData.getBirth());
        response.setIsMan(originData.getIsMan());
        response.setConTime(originData.getConTime());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }
}
