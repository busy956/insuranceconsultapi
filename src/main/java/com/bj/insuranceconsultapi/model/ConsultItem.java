package com.bj.insuranceconsultapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConsultItem {
    private Long id;
    private LocalDate consultDate;
    private String name;
    private String birth;
    private Boolean isMan;
}
