package com.bj.insuranceconsultapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConsultResponse {
    private Long id;
    private LocalDate consultDate;
    private String name;
    private String birth;
    private Boolean isMan;
    private String conTime;
    private String phoneNumber;
}
