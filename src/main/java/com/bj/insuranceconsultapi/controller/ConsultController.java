package com.bj.insuranceconsultapi.controller;

import com.bj.insuranceconsultapi.entity.Consult;
import com.bj.insuranceconsultapi.model.ConsultItem;
import com.bj.insuranceconsultapi.model.ConsultRequest;
import com.bj.insuranceconsultapi.model.ConsultResponse;
import com.bj.insuranceconsultapi.service.ConsultService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/consult")
public class ConsultController {
    private final ConsultService consultService;

    @PostMapping("/people")
    public String setConsult(@RequestBody ConsultRequest request) {
        consultService.setConsult(request);

        return "Ok";
    }

    @GetMapping("/all")
    public List<ConsultItem> getConsults() {
        return consultService.getConsults();
    }

    @GetMapping("/detail/{id}")
    public ConsultResponse getConsult(@PathVariable long id) {
        return consultService.getconsult(id);
    }
}
