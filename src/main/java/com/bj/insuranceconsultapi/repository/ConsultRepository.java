package com.bj.insuranceconsultapi.repository;

import com.bj.insuranceconsultapi.entity.Consult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultRepository extends JpaRepository<Consult, Long> {
}
